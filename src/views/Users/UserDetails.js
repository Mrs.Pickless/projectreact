import React, {Component} from 'react';
import UserService from "../../services/user.services";

export default class UserDetails extends Component{

    constructor(props) {
        super(props);
        this.state = {
            user: {},
            nbrCompleted : "",
            nbrNotCompleted : "",
        }
    }

    async componentDidMount() {
        let response = await UserService.details(this.props.match.params.id);
        this.setState({user: response.data});
        let response2 = await UserService.getNbrTodo(this.props.match.params.id);
        console.log(response2)
        this.setState({nbrCompleted: response2.nbrCompleted ,nbrNotCompleted: response2.nbrNotCompleted });
    }

    async handleDelete(id){
        await UserService.delete(id);
        this.props.history.push('/utilisateurs');
    }

    render() {
        let {user,nbrCompleted,nbrNotCompleted} = this.state;
        return <div className="container">
            <h1>{user.name}</h1>
            <p>Email : {user.email}</p>
            <p>phone : {user.phone}</p>
            <p>tache complété : {nbrCompleted}</p>
            <p>tache non complété : { nbrNotCompleted }</p>

            
            <button className="btn btn-danger" onClick={() => this.handleDelete(user.id)}>Supprimer</button>
        </div>
    }

}
