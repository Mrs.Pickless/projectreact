import React, {Component} from 'react';
import TodoService from "../../services/todo.services";

export default class TodoAdd extends Component{

    constructor(props) {
        super(props);
        this.state = {
            title: null,
            completed: false,
            userId:1

        }
    }

    handleChange(e){
        console.log(  e.target.value)
        this.setState({
            [e.target.id]: e.target.value
        });
    }

    async handleSubmit(e){
        e.preventDefault();
        let {title, completed} = this.state;
        let data = {
            title: title,
            completed: completed,
            userId: 1
        }

        await TodoService.create(data);
        this.props.history.push('/todo');
    }

    render() {
        return <div className="container">
            <h1>Ajouter votre Todo</h1>

            <form onSubmit={(event) => this.handleSubmit(event)}>
                <div className="form-group">
                    <label>Titre</label>
                    <input type="text" id="title" className="form-control" required
                        onChange={(event) => this.handleChange(event)}/>
                </div>
                <button type="submit" className="btn btn-primary">Ajouter</button>
            </form>
        </div>
    }

}
