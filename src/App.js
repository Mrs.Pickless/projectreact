import {BrowserRouter, Route, Switch} from "react-router-dom";
import Header from "./components/Header";
import Footer from "./components/Footer";
import Home from "./views/Home";
import TodoList from "./views/Todos/TodoList";
import TodoAdd from "./views/Todos/TodoAdd";
import TodoDetails from "./views/Todos/TodoDetails";
import UserDetails from "./views/Users/UserDetails";
import TodoUpdate from "./views/Todos/TodoUpdate";
import UserList from "./views/Users/UsersList";



function App() {
  return (
<BrowserRouter>
            <Header/>
            <Switch>
                <Route exact path="/" component={Home} />
                <Route exact path="/todo" component={TodoList} />
                <Route exact path="/todo/add" component={TodoAdd} />
                <Route exact path="/todo/:id/details" component={TodoDetails} />
                <Route exact path="/user" component={UserList} />

                <Route exact path="/todo/:id/update" component={TodoUpdate} />



                <Route exact path="/user/:id/details" component={UserDetails} />

            </Switch>
            <Footer/>
        </BrowserRouter>
  );
}

export default App;
