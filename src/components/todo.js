import React, {Component} from 'react';
import {Link} from "react-router-dom";

export default class Todo extends Component{

    render() {
        let {Todo} = this.props;
        return <div className="card">
                <div className="card-body">
                    <h5 className="card-title">{Todo.title}</h5>
                    <p className="card-text">{Todo.completed.toString()}</p>
                    <p className="card-text">{Todo.user.name}</p>
                    <Link to={`/todo/${Todo.id}/details`} className="btn btn-primary">Détails</Link>
                    {/* <Link to={`/articles/${post.id}`} className="btn btn-primary">Détails</Link>
                    <Link to={`/articles/${post.id}/modifier`} className="btn btn-warning">Modifier</Link> */}
                </div>
        </div>
    }
}
