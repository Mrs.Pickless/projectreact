import React, {Component} from 'react';

export default class Footer extends Component{

    render() {
        return <footer className="bg-light text-center text-lg-start">

        <div className="container p-4">

          <div className="row">

            <div className="col-lg-6 col-md-12 mb-4 mb-md-0">
              <h5 className="text-uppercase">Footer Content</h5>
      
              <p>
                Information du client
              </p>
            </div>

            <div className="col-lg-12 col-md-12 mb-12 mb-md-12">
              <h5 className="text-uppercase mb-0">Réseaux sociaux</h5>
      
              <ul className="list-unstyled">
                <li>
                  <a href="#!" className="text-dark">facebook</a>
                </li>
                <li>
                  <a href="#!" className="text-dark">Twitter</a>
                </li>
                <li>
                  <a href="#!" className="text-dark">Instagram</a>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div className="text-center p-3">
          © 2020 Copyright:
          <a className="text-dark" href="https://mdbootstrap.com/">NomDuClient.com</a>
        </div>
      </footer>
    }
}
